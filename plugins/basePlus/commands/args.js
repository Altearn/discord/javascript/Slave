module.exports.run = [
    main = (client, message, args) => {
        message.channel.send(`Arguments : \`${args.join("\`, \`")}\``);
    }
];

module.exports.help = {
    name: "args",
    desc: "say all args of the command",
    aliases: [""],
    usage: "[<arg>]"
}