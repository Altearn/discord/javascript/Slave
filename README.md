> Gunibot Instance Slave

**How to setup your own instance of GuniSlave :**

- Download this repository
- Run the command `npm i`
- Config your bot in the file named `setup.json`)
- Run it with `node index` (or with pm2 as you want and can) and here you go

Enjoy your new little assistante
